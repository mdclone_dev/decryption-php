<?php
DEFINE('DS', DIRECTORY_SEPARATOR);

class Logger {
    public static function log($msg, $status = "DEBUG", $func = "MAIN") {
        $dateTimeNow = date("Y-m-d H:i:s");
        $today = date("j.n.Y");
        $status = strtoupper($status);
        $func = strtoupper($func);
        $msg = "[$dateTimeNow] [$func] [$status] - " . $msg .PHP_EOL;
        file_put_contents(dirname(__FILE__) . DS . 'logs\log_'. $today .'.log', $msg, FILE_APPEND);
    }

     public static function debug($msg, $func = "MAIN") {
        self::log($msg, $status = "DEBUG", $func = "MAIN");
    }

    public static function error($msg, $func = "MAIN") {
        self::log($msg, $status = "ERROR", $func);
    }
}

class Decryption {

    public $db;
    public $data;
    public $dbConfig;
    public $appVersion;
    public $mdcloneSharedFolder;
    public $nodeHost;
    public $nodePort;
    private $decode;
    private $requestUsername;
    
    public function __construct() {
        $servername = "10.0.2.42";
		$port = "5432";
        $username = "postgres";
        $password = "Ppp@24680";
        $dbname = "mdclone";
        $nodeHost = "10.0.2.62";
        $nodePort = "7676";
        $mdcloneSharedFolder = "E:\mdclone_shared";

        try {
            $this->db = $pdo = new PDO("pgsql:dbname=$dbname;host=$servername;port=$port", $username, $password );
        } catch (PDOException $e) {
            Logger::error($e->getMessage(), "postgres");
            echo $e->getMessage();
            die;

        }
        $this->dbConfig = $this->getConfig();
        $this->appVersion = $this->dbConfig['decryption']['app_version'];
        $this->data = $this->getUrlParam();
        $this->requestUsername = strtolower($this->data["user"]);
        $this->mdcloneSharedFolder = $mdcloneSharedFolder;
        $this->nodeHost = $nodeHost;
        $this->nodePort = $nodePort;
        if ($this->appVersion == "5")
            $this->decode = $this->decode();
    }

    private function getUrlParam() {
        $data = [];
        switch ($this->appVersion):
            case ("5"):
                foreach($_GET as $key => $value) {

                    $data[$key] = $value;

                }
                $data['url'] = $data['file'];
                $parts = parse_url($data['file']);
                parse_str($parts['query'], $query);
                $data['file'] = $query['file'];
                break;
            case ("6"):
                foreach($_GET as $key => $value) {

                    $data[$key] = $value;

                }
                $parts = parse_url($data['file']);
                $url_parts = explode("/", $parts["path"]);
                $data["url"] = $parts["path"];
                $data['session_id'] = end($url_parts);
                break;
        endswitch;

        return $data;
    }

    private function getConfig() {
        $stm = $this->db->query("SELECT * from common.mdc_config");
        $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
        $data = [];
        foreach($rows as $row) {
            $data[$row['program']][$row['parameter_name']] = $row['parameter_value'];
        }

        return $data;
    }

    public function decode() {
        $file = str_replace(" ", "+", $this->data['file']);
        $file = urldecode(openssl_decrypt($file, "AES-256-CBC", $this->getPrivateKey(), 0, $iv = "0123456789abcdef"));
        $path['original'] = $file;
        $path['params']=explode("\\",$file);
        return $path;
    }

    private function getPrivateKey() {
        return $this->dbConfig['general']['private_key'];
    }

    function get_content($user=null){
        if($user) {
            $term = parse_url($this->data['file']);
            $term['host'] = $this->nodeHost;
            $term['port'] = $this->nodePort;
            $this->data['file'] = $this->unparse_url($term);
            // Create a stream
            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'header'=>"user: session:$user\r\n"
                )
            );
            $context = stream_context_create($opts);
            $data = file_get_contents($this->data['file'], false, $context);
        } else {
            $data = file_get_contents($this->decode['original']);
        }
        return $data;
    }

    function unparse_url(array $parsed): string {
        $pass      = $parsed['pass'] ?? null;
        $user      = $parsed['user'] ?? null;
        $userinfo  = $pass !== null ? "$user:$pass" : $user;
        $port      = $parsed['port'] ?? 0;
        $scheme    = $parsed['scheme'] ?? "";
        $query     = $parsed['query'] ?? "";
        $fragment  = $parsed['fragment'] ?? "";
        $authority = (
            ($userinfo !== null ? "$userinfo@" : "") .
            ($parsed['host'] ?? "") .
            ($port ? ":$port" : "")
        );
        return (
            (\strlen($scheme) > 0 ? "$scheme:" : "") .
            (\strlen($authority) > 0 ? "//$authority" : "") .
            ($parsed['path'] ?? "") .
            (\strlen($query) > 0 ? "?$query" : "") .
            (\strlen($fragment) > 0 ? "#$fragment" : "")
        );
    }

    public function main() {
        switch ($this->appVersion) :
            case ("5"):
                $csvData = $this->get_content();
                $tmpPath = $this->dbConfig['general']['app_files_path'];
                $sessionId = $this->decode['params'][4];
                $date = date("Ymd_hms");
                $fileName = "[Decrypted][" . $this->data["user"] . "][" . $sessionId . "][" . $date . "].csv";
                $fullPath = $tmpPath . "\\tmp\\" . $fileName;
                if (strpos($this->decode['original'], "[Synthetic]") !== false) {
                    Logger::error(print_r(strpos($this->decode['original'], "[Synthetic]"), true));
                    echo "You Cant Decrypt Synthetic File";
                    die;
                }
                $fileData = file($this->decode['original']);
                $firstRowArray = explode(',', str_replace('"','', $fileData[0]));
                $fileFirstRow = $fileData[0];
                $result = file_put_contents($fullPath, $csvData);
                if (!$result) {
                    Logger::error(print_r($fullPath, true));
                    echo "Destination File Doesnt Exists";
                    die;
                }
                $colsNumber = count($fileFirstRow);
                $foreignTableName = "decrypted_file_" . $this->decode['params'][4] . "_" . preg_replace('/[\.\-]/', '_', $this->requestUsername) . "_" . $date;
                $foreignTable = $this->createForeignTable($fullPath, $fileFirstRow, $foreignTableName);
                if ($foreignTable) {
                    $decryptedData = $this->getDecryptedData("tmp_data", $foreignTableName, $sessionId);
                    $this->array_to_csv_download($decryptedData, $fileName);
                } else {

                }
                break;
            case ("6"):
                $csvData = $this->get_content($this->data["user"]);
                $tmpPath = $this->mdcloneSharedFolder;
                $sessionHistoryID = $this->data['session_id'];
                $sessionID = $this->getSessionId("public", "mdc_session_history", $sessionHistoryID);
                $date = date("Ymd_hms");
                $fileName = "[Decrypted][" . $this->data["user"] . "][" . $sessionID . "][" . $date . "].csv";
                $fullPath = $tmpPath . "\\tmp\\" . $fileName;
                if (strpos($this->data['file'], "download-synthetic") !== false) {
                    Logger::error(print_r(strpos($this->data['file'], "download-synthetic"), true));
                    echo "You do not have permission to use this application. Please contact your administrator.";
                    die;
                }
                $fileOriginalPath = $this->getFilePath("public", "mdc_session_history_files", $sessionHistoryID, "8");
                $result = file_put_contents($fullPath, $csvData);
                if (!$result) {
                    Logger::error(print_r($fullPath, true));
                    echo "Destination File Doesnt Exists";
                    die;
                }
                $fileData = file($fullPath);
                $fileFirstRowArray = explode(",", $fileData[0]);
                $fileNewFirstRowArray = [];
                foreach ($fileFirstRowArray as $key => $value) {
					if ($this->str_ends_with($value, "-encrypted_date_shift_value") || 
						$this->str_ends_with($value, "-encrypted_id") ||
						$this->str_ends_with($value, "-age_at_event") ||
						$this->str_ends_with($value, "-event_date-days from reference") ||
						$this->str_ends_with($value, "-internalpatientid")) 
					{
						$fileNewFirstRowArray[$key] = $value;
					} else {
                        $fileNewFirstRowArray[$key] = "H_" . $key;
                    }
                }
                $fileFirstRow = '"' . implode('", "' , $fileNewFirstRowArray) . '"';
                $foreignTableName = "decrypted_file_" . $sessionID . "_" . preg_replace('/[\.\-]/', '_', $this->requestUsername) . "_" . $date;
                $foreignTable = $this->createForeignTable($fileOriginalPath, $fileFirstRow, $foreignTableName);
				if ($foreignTable) {
                    $decryptedData = $this->getDecryptedData("tmp_data", $foreignTableName, $sessionHistoryID);
					foreach ($decryptedData[0] as $key => $value) {
						if (preg_match('/H_(\d+)_decrypted$/', $key, $matches)) {
							$number = isset($matches[1]) ? $matches[1] : null;
							$decryptedData[0][$key] = $fileFirstRowArray[$number] . "_decrypted";
						} elseif (preg_match('/H_(\d+)/', $key, $matches)) {
							$number = isset($matches[1]) ? $matches[1] : null;
							$decryptedData[0][$key] = $fileFirstRowArray[$number];
						}
					}
                    $this->array_to_csv_download($decryptedData, $fileName);
                } else {
                    Logger::error(print_r($foreignTable, true));
                    echo "You Cant Decrypt Synthetic File";
                    die;
                }
                break;

        endswitch;
        $this->deleteForeignTable("tmp_data", $foreignTableName);
        die;
    }
	
	private function str_ends_with($string, $subString) {
		$result = substr_compare($string, $subString, -strlen($subString)) === 0;
		
		return $result;
	}

    private function createForeignTable($fullPath, $firstRow, $foreignTableName) {
        $sql = "select * from common.usp_mdc_create_foreign_table_with_origin_col_names ('" . $fullPath . "', '" . $firstRow . "', '" . $foreignTableName . "' )";
        Logger::debug(print_r($sql, true), "createForeignTable");
        $stm = $this->db->query($sql);
        $error = $this->db->errorInfo();
        if ($error[0] == "0000") {
            $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
            if ($rows[0]["usp_mdc_create_foreign_table_with_origin_col_names"] == "0") {
                Logger::error(print_r($error, true), "createForeignTable");
                echo "createForeignTable : returns " . $rows[0]["usp_mdc_create_foreign_table_with_origin_col_names"];
                die;
            }
        } else {
            Logger::error(print_r($error, true), "createForeignTable");
            echo $error[2];
            die;
        }
        if (isset($rows[0]["usp_mdc_create_foreign_table_with_origin_col_names"])) {
            return true;
        } else {
            return false;
        }
    }

    private function getDecryptedData($schema, $table, $sessionId) {
        $sql = "select * from common.usp_mdc_get_decrypted_data ('" . $schema . "', '" . $table . "', '" . $sessionId . "' )";
        Logger::debug(print_r($sql, true), "getDecryptedData");
        $stm = $this->db->query($sql);
        $error = $this->db->errorInfo();
        if ($error[0] == "0000" ) {
            $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
        } else {
            Logger::error(print_r($error, true), "getDecryptedData");
            echo "You do not have permission to use this application. Please contact your administrator.";
            die;
        }

        if (isset($rows[0]["usp_mdc_get_decrypted_data"])) {
            $sql = $rows[0]["usp_mdc_get_decrypted_data"];
            Logger::debug(print_r($sql, true), "getDecryptedData");
            $stm = $this->db->query($sql);
            $error = $this->db->errorInfo();
            if ($error[0] == "0000") {
                $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
            } else {
                Logger::error(print_r($error, true), "getDecryptedData");
                echo $error[2];
                die;
            }
            $headers = $this->buildHeaders($rows[0]);
            array_unshift($rows, $headers);

            return $rows;
        } else {
            Logger::error(print_r("no data", true), "getDecryptedData");
            echo "no data";
            die;
        }
    }

    private function buildHeaders($array) {
        foreach ($array as $key => $value) {
            $newArray[$key] = $key;
        }
        return $newArray;
    }

    private function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
        Logger::debug(print_r($filename, true), "array_to_csv_download");
        header('Content-Type: application/csv, charset=UTF-8');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        // open the "output" stream
        // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
        $f = fopen('php://output', 'w');
        $first = true;
        foreach ($array as $line) {
            if ($first) {
                fprintf($f, chr(0xEF).chr(0xBB).chr(0xBF));
				$line = preg_replace("/[\r\n]*/","",$line);
                $first = false;
            }
            fputcsv($f, $line, $delimiter);
        }
        fclose($f);
    }

    private function deleteForeignTable($schema, $table) {
        $sql = "drop foreign table if exists " . $schema . "." . $table;
        $stm = $this->db->query($sql);
        $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }

    private function getSessionId($schema, $table, $session_history_id) {
        $sql = "select session_id from " . $schema . "." . $table . " where session_history_id=" . $session_history_id . " limit 1";
        $stm = $this->db->query($sql);
        $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

        return $rows[0]["session_id"];
    }

    private function getFilePath($schema, $table, $session_history_id, $file_type) {
        $sql = "select * from " . $schema . "." . $table . " where session_history_id=" . $session_history_id . " AND file_type_id=" . $file_type;
        $stm = $this->db->query($sql);
        $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

        $filePath = $rows[0]["file_path"] . "/" . $rows[0]["file_name"];

        return $filePath;
    }
}

$run = new Decryption();
$run->main();
die;

?>
